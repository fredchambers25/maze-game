# Maze Game

Mazes have essentially five possible purposes: to escape, to get the prize, to trace a path, to be a metaphor, to be a stage. Escape: By far the most common purpose of a maze is to challenge the visitor to find the means of passing through and escaping. This is true of pencil and paper mazes and most hedge mazes.

